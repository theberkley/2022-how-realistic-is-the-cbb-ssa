% chktex-file 8
\documentclass[num-refs]{wiley-article}
\usepackage{siunitx}
\usepackage{anyfontsize}
\usepackage{glossaries}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{xr}
\usepackage{float}
\usepackage[nameinlink,noabbrev,capitalize]{cleveref}
\usepackage[section]{placeins}
\usepackage[parfill]{parskip}  % avoid indents
\externaldocument[supp-]{supplementary}  % always build supplementary.tex first for this to work


\papertype{Original Article}
\paperfield{Journal Section}

% \title{Modelling carbon fixation - how much information do we need?}
\title{Approximating carbon fixation - how realistic is the CBB cycle steady-state assumption?}

\author[1]{Marvin van Aalst}
\author[2,3,4]{Berkley J. Walker}
\author[1,4]{Oliver Ebenhöh}

\affil[1]{
    Institute of Theoretical and Quantitative Biology,
    Heinrich Heine University Düsseldorf,
    Düsseldorf, Germany}
\affil[2]{
    Department of Energy-Plant Research Laboratory,
    Michigan State University,
    East Lansing, MI, USA}
\affil[3]{
    Department of Plant Biology,
    Michigan State University,
    East Lansing, MI, USA}
\affil[4] {
    Cluster of Excellence on Plant Sciences,
    Heinrich Heine University Düsseldorf,
    Düsseldorf, Germany}
\fundinginfo{
    This work was funded by the Deutsche Forschungsgemeinschaft Research
    (DFG, German Research Foundation) under Germany’s Excellence Strategy - EXC-2048/1 - project ID 390686111
    and DFG Grant MA 8103/1-1 project ID 420069095.}

\corraddress{Someone}
\corremail{someone@something.com}

\presentadd[\authfn{2}]{Department, Institution, City, State or Province, Postal Code, Country}

\runningauthor{van Aalst et al.}

\newacronym{cbb}{CBB}{Calvin-Benson-Bassham}
\newacronym{petc}{PETC}{photosynthetic electron transfer chain}
\newacronym{rmse}{RMSE}{root-mean-square error}
\newacronym{mehler}{WWC}{water-water cycle}
\newacronym{pfd}{PPFD}{photosynthetically active photon flux density}
\newacronym{rubisco}{RuBisCO}{Ribulose-1,5-bisphosphate carboxylase-oxygenase}
\newacronym{sbpase}{SBPase}{sedoheptulose-bisphosphatase}
\newacronym{ode}{ODE}{ordinary differential equation}
\newacronym{co2}{CO2}{carbon dioxide}
\newacronym{neon}{NEON}{National Ecological Observatory Network}
\newacronym{lbfgs}{L-BFGS-B}{Limited-memory Broyden-Fletcher-Goldfarb-Shanno}
\newacronym{rubp}{RuBP}{Ribulose-1,5-bisphosphate}
\newacronym{e4p}{E4P}{erythrose-4-phosphate}
\newacronym{fbp}{FBP}{fructose-1,6-bisphosphat}
\newacronym{sbp}{SBP}{sedoheptulose-1-7-bisphosphate}
\newacronym{s7p}{S7P}{sedoheptulose-7-phosphate}
\newacronym{x5p}{X5P}{xylulose-5-phosphate}
\newacronym{r5p}{R5P}{ribose-5-phosphate}
\newacronym{atp}{ATP}{adenosine-triphosphate}
\newacronym{nadph}{NADPH}{Nicotinamide adenine dinucleotide phosphate}

\glsunset{atp}  % suppress expansion, these are clear
\glsunset{nadph} % suppress expansion, these are clear

% \newacronym{dss}{SS model}{steady-state approximated model}

\begin{document}

\begin{frontmatter}
    \maketitle{}
    \begin{abstract}
        This is some placeholder text. But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?
    \end{abstract}
\end{frontmatter}




\section{Introduction}

Biological systems are complex.
To make these systems comprehensible, the need for mathematical models and simplifications arises.
% While using drawings or analogies as models in Biology has a rich history, strict mathematical models have only been made possible with the advent of the computer.
Which simplifications are appropriate depends on the question asked, as all models are wrong, but some are useful.
To validate the constructed model, its predictions are usually tested against measured data, but that measured data can be very hard to obtain.
For example, getting concentrations over time in a biological system can require severe invasion into that system, rendering the measurement invalid.
Other metabolites may have very low concentrations in the system, making them hard to measure whilst measuring other metabolites of interest at the same time.
What is to be done in those cases?
One possible solution occurs when a mathematical model already exists, that has been validated on a related - but usually harder problem.
In that case the model can be reduced until the simplest possible model is identified that still gives a robust prediction of the quantities one is interested in.
This model reduction can greatly aid in the comprehension of the system, as it elucidates the main processes controlling it.
Here we present an exploration of the main processes controlling carbon fixation rate found in a previously published model of the \gls{petc} and the \gls{cbb} cycle.
% While this model is not meant to be used for canopy-scale simulations, it does yield some interesting results related to it.


\section{Results}

\subsection*{Reducing the model complexity}
We based our investigations on \gls{pfd} field observation data measured in one minute intervals in Washington state and an \gls{ode} model originally build to study the interplay between the \gls{cbb} cycle, \gls{petc} and photoprotective mechanisms \cite{neondata, saadat2021}.
As the interactions studied before require high fidelity, the reaction mechanisms are described in great detail, with 46 reactions and 194 parameters.
% All this added detail can make numerical integration slow or even unstable if the inputs to the system are varying very quickly, as can be the case with the measured PFD values.
To simplify the model we removed the dynamic change of metabolite concentrations by pre-calculating steady-state fluxes for the entire range of \gls{pfd} values found in the data.
We then predicted the fluxes over time by looking up the steady-state flux of the respective \gls{pfd}.
For this initial experiment we isolated a slice of 6 hours from a randomly selected summer day from the \gls{pfd} data.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h]
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part1-flux-over-time.png}
    \caption{Comparison of \gls{ode} and steady-state approximated model predictions.
    Upper left: time course over 6 hours showing the \gls{rubisco} flux.
    Upper right: error of steady-state approximated model \gls{rubisco} predictions relative to \gls{ode} model predictions.
    }
    \label{fig:experiment}
\end{figure}

\cref{fig:experiment} shows the rate of \gls{rubisco} in both the \gls{ode} model and the steady-state approximated model of the left-hand-side and the relative error of the steady-state approximated model in comparison to the \gls{ode} model on the right-hand side.
While the relative error of \gls{rubisco} flux in short periods can exceed 50 \%, the total error of predicting the \gls{rubisco} flux is 0.49 \%.
In order to understand why the steady-state approximated model behaves so similarly to the dynamic \gls{ode} model with regard to total carbon fixation, we investigated how well other fluxes are predicted and much the concentrations change over the course of the experiment.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[ht]
    \centering{}
    \begin{subfigure}[b]{0.45\linewidth}
        \centering{}
        \includegraphics[width=\linewidth]{figures/part1-abs-rel-tot-error-selection.png}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\linewidth}
        \includegraphics[width=\linewidth]{figures/part1-rel-std-concs-selection.png}
    \end{subfigure}
    \caption{
    Left: absolute total error of steady-state approximated model predictions compared to \gls{ode} model predictions.
    Right: relative standard deviation (coefficient of variation) of \gls{ode} model predicted metabolite concentrations over a course of a 6-hour experiment.}
    \label{fig:bar-charts}
\end{figure}

For this we calculated the absolute total error of the predictions of selected fluxes on the left-hand side and the relative standard deviation (coefficient of variation) of a selection of concentrations in the \gls{ode} model over the time of the experiment on the right-hand side, shown in \cref{fig:bar-charts}.
The remaining fluxes and concentrations are displayed in supplementary \cref{supp-fig:abs-rel-tot-error-full,supp-fig:rel-std-concs-full} respectively and the relative standard deviation for the fluxes in supplementary \cref{supp-fig:rel-std-fluxes-full}.
Contrasting the small error of \gls{rubisco} predictions, the error of predicting the flux of rapid acclimation response mechanisms such as the xanthophyll cycle (vEpox, vAscorbate, vDeepox) or water-water cycle (v3ASC, vDHAR, vGR) is between 20 \% and 50 \%.
Similarly, the relative standard deviation of the \gls{rubisco} substrate \gls{rubp} is comparatively small, varying 15 \% of its mean value in comparison to the large relative standard deviation of some metabolites that take part in rapid acclimation response, e.g. GSSG \& DHA vary nearly up to 350 \% of their mean value.
Notably, other \gls{cbb} cycle intermediates like \gls{sbp} and \gls{s7p} can vary up to 50 \% while the error of the flux prediction of \gls{sbpase} is comparable to the one of \gls{rubisco}.
% of their mean value, while the other pentoses like \gls{x5p} and \gls{r5p}
% as well as energy metabolites like \gls{atp} and \gls{nadph} vary even less than \gls{rubp}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure 3 (supplementary)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Given the previously presented stability of the \gls{rubp} concentration during our simulations, we hypothesised that it is possible to remove the entire \gls{cbb} cycle from the model and still get a reasonable prediction.
We thus calculated the predicted steady-state \gls{rubisco} flux over a range of \gls{pfd} values together with Michaelis-Menten kinetics and on a polynomial of degree four fitted to the predicted flux, shown in supplementary \cref{supp-fig:rubisco-approximations}.
While the Michaelis-Menten approximation has a \gls{rmse} of 0.22 \(\frac{mM}{s}\), the 4th degree polynomial has a \gls{rmse} of 0.02 \(\frac{mM}{s}\).
If simulated over the same data shown in \cref{fig:experiment}, the error of the total \gls{rubisco} flux of the polynomial in comparison to the predicted \gls{ode} fluxes is 0.64 \% and thus comparable to the 0.49 \% error of the steady-state approximation, while the Michaelis-Menten approximation has an error of 1.5 \%, both shown in supplementary \cref{supp-fig:rubisco-approximations-prediction}.


\subsection*{Reducing the data requirement}

So far we have used high temporal resolution data in our simulations.
But data availability, lack of computational power or technical difficulties due to multiscale modelling can require the use of low temporal resolution data.
A common practice is to use the average \gls{pfd} value over a longer period of time.
An increase from one minute steps to 60 minute steps increased the carbon fixation prediction error from 0.49 \% to roughly 7 \% (see supplementary \cref{supp-fig:error-by-step-size}).
While an increase in error with fewer data is to be expected, we hypothesised that the error can be increased substantially.
As carbon fixation saturates in our model, and thus the function of \gls{pfd} to carbon fixation is non-linear, \gls{pfd} values above the saturation limit in the input data will lead to an over-prediction of the carbon fixation rate if the average of a data set is used.
To counteract this bias, we clipped the input data above a certain threshold and then calculated the mean \gls{pfd}.
As this led to promising results with our initial data we expanded the analysis to data of the entire year.
For this we identified which day had the highest data coverage for each month, which was the 25th, and then simulated the carbon fixation rate of this day for each month with one minute steps for the \gls{ode} model and 60 minute steps for both the steady-state approximated model and the polynomial model.
For the latter two we included both the prediction with the mean of the raw data and the mean of the clipped data.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure 4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[h]
    \centering{}
    \includegraphics[width=0.9\linewidth]{figures/part2-absolute-total-error-per-day-selection.png}
    \caption{Total prediction error of carbon fixation of the steady-state approximated model (60 minute resolution) and the polynomial model (60 minute resolution) relative to the \gls{ode} model (1 minute resolution). Results are shown for both the mean \gls{pfd} value of the raw data and the mean value of the data clipped at either \gls{pfd} 900 (steady-state approximated model) or 1000 (polynomial model).}
    \label{fig:error-per-month}
\end{figure}

\cref{fig:error-per-month} shows the total error of the \gls{rubisco} flux prediction of the steady-state approximated model and the polynomial model relative to the \gls{ode} model per month.
As indicated in the figure legend, the relative total error per year for the steady-state approximated model is 6.1 \% for raw data and 0.7 \% for data clipped at a \gls{pfd} of 900.
The polynomial model shows an error of 4.0 \% for raw data and 0.1 \% for data clipped at a \gls{pfd} of 1000.
The absolute of the error of the polynomial model for October to February is between 5 and 15 \%, while it is lower for the summer months (except June).
The absolute error of the steady-state approximated model can be nearly 15 \% per month for the raw data, but is lower than 5 \% for every month for the data clipped at a \gls{pfd} of 1000 and in both cases fits way better than the polynomial model for the winter months.

\section{Discussion}

This study suggests that for the purpose of predicting total carbon fixation over a given period of time the complex and dynamic \gls{ode} model can be replaced by either its steady-state approximated version or even a simple polynomial function of degree four.
The reason why the model can be simplified this drastically is that the plant metabolism approximated by the \gls{ode} model regulates itself in a way that the \gls{cbb} cycle can be approximated as being in steady-state.
This approximation holds because even though some \gls{cbb} cycle intermediates like \gls{sbp} can change up to 50 \% of their mean value over time, see \cref{fig:bar-charts}, the respective relative standard deviation of the flux over time of almost all \gls{cbb} cycle reactions is similar.
This means that the entire cycle seems to sync up in accelerating or decelerating upon changes in energy supply.
So while the \gls{cbb} cycle is not, and should not be assumed to be in a single steady-state, this study suggests that it is always close to the respective steady-state of the current illumination.

Further we could show that high-resolution \gls{pfd} data is not necessarily required to give a good prediction of total carbon fixation, as long as the data is modified to account for the saturating nature of \gls{rubisco}.
In that case, both the steady-state approximated model and the polynomial model give predictions with less than 1 \% error over the entire year, even though the \gls{pfd} was held constant over 60 minute intervals.
The polynomial model consistently under-predicts the carbon fixation rate of the winter months, however as these months have less carbon fixation than the summer months in the given climate, this does not influence the total carbon fixation by much.

We would like to stress that these results do not mean that carbon fixation can just be described with a single fitted polynomial in general.
There are effects like temperature and \gls{co2} concentration that our initial \gls{ode} model does not capture, and thus the reduction does not need to model.
If those effects can be shown to lead to less stable \gls{rubp} concentration, the steady-state assumption would be invalid and thus the results as well.
What we would like to stress however, is the point that models should be build with a specific goal in mind instead of unnecessarily including processes which do contribute only in a minor way to the overall results.

% with regard to \gls{rubisco} flux.
% the concentration of \gls{rubp} changes very little over time, ca 15 \% of the mean concentration.
% This would be different if we were interested in other parts of the system, as the remainder of the \gls{cbb} cycle is not necessarily in steady-state, seen for example in the \gls{sbp} concentration, which varies more than 50 \% of its mean concentration over time.
% However, for the purpose of predicting total carbon fixation the non-steady-state nature of other cycle intermediates is not important as long as their influence is buffered by the remaining cycle, which is the case in this model.


\section{Methods}

We performed our analyses in Python 3.10, utilising the common packages NumPy, pandas, SciPy and Matplotlib  for general data analysis as well as modelbase and Assimulo for building and integrating the \gls{ode} model \cite{python310,numpy, pandas, scipy, matplotlib, modelbase, assimulo}.
All code used to generate the publication results and figures is publicly available on our GitLab repository \url{https://gitlab.com/marvin.vanaalst/model-assumption-assessment}.
We obtained field observation \gls{pfd} data measured in one minute intervals in Washington state (latitude 45.790835, longitude -121.933788) by the \gls{neon} and the \gls{ode} model from Saadat 2021 \cite{neondata, saadat2021}.
Due to model instabilities for \gls{pfd} value below 30, we clamped the data below 30 to 30.

\subsection*{Steady-state model}
We calculated the steady state fluxes of the \gls{ode} model for \gls{pfd} values between the minimal and maximal values found in the dataset with step size 1.
Then we simulated the model by looking up the steady state flux of the \gls{pfd} value rounded to the nearest integer.

\subsection*{Approximations}

We fitted the \(V_{max}\) and \(K_m\) parameters of the Michaelis-Menten function using the SciPy \texttt{minimize} function and the L-BFGS-B algorithm \cite{scipy, lbfgsb}.
The polynomial fit was performed using NumPy's \texttt{polyfit} function \cite{numpy}.

\bibliography{bib}

\end{document}