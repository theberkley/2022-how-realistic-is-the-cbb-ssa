# Approximating carbon fixation - how realistic is the CBB cycle steady-state assumption?

## Conclusions

- The C3 cycle is structured such that net CO2 fixation can be accurately simulated, even if the underlying metabolism is not(This is an important point since many models are validated just by the ability to predict net assimilation and not to represent metabolite concentrations)

- ODE models can be replaced with simpler non steady-state models to describe net CO2 fixation under fluctuating light conditions

- The ability of simpler models to capture the behaviour of saturating responses to light can be improved by clipping data before averaging

## Discussion

1. Steady-state nature of the CBB cycle
   1. Fluxes of rapid acclimation mechanisms are predicted badly
   2. Some CBB cycle intermediates can vary a lot, but RuBP is stable
      3. This paper is probably of interest: 1.	Arnold, A.; Nikoloski, Z., A quantitative comparison of Calvin-Benson cycle models. Trends Plant Sci 2011, 16 (12), 676-683.
   3. We thus hypothesized that we can remove entire CBB cycle
   4. A low-degree polynomial can replace the entire CBB cycle, as prediction only depends on PPFD
   5. Future work can test this with more complex models of carbon assimilation that include photorespiration
   6. This observation highlights that we need to be careful to simply use rates of carbon fixation to validate a model since we know a model can be very wrong and still give similar rates of carboxylation

2. Model reduction
   1. The ability of the CBB cycle to have wildly differently modeled pool sizes, but similar rates of net assimimilation means that it can be simpified
   2. Remove dynamic change of metabolite concentrations
   3. Dynamic error can be high
   4. Curiously, total error of RuBisCO is low
   5. Can we say anything about ODE models in general from this?

3. Mean transformations for saturating kinetics
   1. Often less data is available / only a limited set can be used
      2. Different types of data can also be gathered at different intervals, for example irradiance can be captured at time scales of seconds while eddy covariance data requeres at least 30 minute integrations.
   2. Taking the mean over the data increases prediction error by ~14x due to weighted mean
   3. Robust fix for that is to clip the data above a certain threshold
   4. Clipped models outperform models working on raw data
      1. Distill this into a clear recommendation
